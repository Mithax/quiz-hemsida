package com.idnoll.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idnoll.models.CategoryModel;
import com.idnoll.repositories.CategoryRepository;

@Service
public class CategoryService {
	
	
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	
	public List<CategoryModel> findAllMenuCategory(){
		List<CategoryModel> listOfAllCategories = categoryRepository.findAll();
		System.out.println("Getting all categories from database: ");
		
		return listOfAllCategories;
	}

	public CategoryModel findOne(Long menu_id) {
		CategoryModel categoryModel = categoryRepository.findOne(menu_id);
		System.out.println("Getting categoryModel from database: " + categoryModel.toString());
		return categoryModel;
	}
}
