package com.idnoll.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idnoll.models.MenuModel;
import com.idnoll.repositories.MenuRepository;

@Service
public class MenuService {

	

	@Autowired
	private MenuRepository menuRepository;

	public List<MenuModel> findAllMenuCategory() {

		List<MenuModel> listOfAllMenuModels = menuRepository.findAll();
		System.out.println("Getting all menuItems from database: ");
		
		return listOfAllMenuModels;
	}

	public MenuModel findOne(int menu_id) {
		MenuModel menuModel = menuRepository.findOne(menu_id);
		System.out.println("Getting menuModel from database: " + menuModel.toString());
		return menuRepository.findOne(menu_id);

	}
}
