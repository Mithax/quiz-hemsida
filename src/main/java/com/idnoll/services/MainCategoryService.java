package com.idnoll.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idnoll.models.CategoryModel;
import com.idnoll.repositories.CategoryRepository;

@Service
public class MainCategoryService {

	

	@Autowired
	CategoryRepository categoryRepository;

	public CategoryModel createCategory(CategoryModel categoryModel) {
		System.out.println("Adding category to database: " + categoryModel.toString());
		return categoryRepository.saveAndFlush(categoryModel);

	}

	public List<CategoryModel> getAllCategories() {
		List<CategoryModel> listOfAllCategoryModels = categoryRepository.findAll();

		System.out.println("Getting all categories from database: ");
		
		return listOfAllCategoryModels;

	}

	public void deleteCategory(CategoryModel categoryModel) {
		System.out.println("Deleting category from database: " + categoryModel.toString());
		categoryRepository.delete(categoryModel);
	}

}
