package com.idnoll.services;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idnoll.repositories.CategoryRepository;
import com.idnoll.repositories.UnderCategoryRepository;

@Transactional
@Service
public class InitDbService {

	@Autowired
	private CategoryRepository menuRepository;

	@Autowired
	private UnderCategoryRepository undercategoryRepository;

	@PostConstruct
	public void init() {
		// THIS IS MEANT TO BE USED WHEN THE ADMIN WANTS TO CREATE NEW
		// CATEGORIES AND UNDERCATEGORIES
		// MenuModel categoryMenu = new MenuModel();
		// categoryMenu.setCategories_name("Sport");
		// menuRepository.save(categoryMenu);
		//
		// MenuModel menuCategory = new MenuModel();
		// menuCategory.setCategories_name("Geografi");
		// menuRepository.save(menuCategory);
		//
		// UndercategoryModel undercategoryMenu = new UndercategoryModel();
		// undercategoryMenu.setUndercategories_name("Skandinavien");
		// List<UndercategoryModel> undercategories = new
		// ArrayList<UndercategoryModel>();
		// undercategories.add(undercategoryMenu);
		// undercategoryRepository.save(undercategoryMenu);
	}
}
