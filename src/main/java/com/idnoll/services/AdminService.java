package com.idnoll.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.idnoll.models.QuestionModel;
import com.idnoll.repositories.QuestionRepository;

@Service
public class AdminService implements AdminServiceInterface {

	

	@Autowired
	private QuestionRepository questionRepository;

	@Override
	public QuestionModel createQuestion(QuestionModel questionModel) {
		System.out.println("Saving questionmodel to database: " + questionModel.toString());
		return questionRepository.save(questionModel);
	}

	@Override
	public QuestionModel getQuestion(Long id) {
		QuestionModel questionModel = questionRepository.findOne(id);
		System.out.println("Getting questionmodel from database: " + questionModel.toString());
		return questionModel;
	}

	@Override
	public void updateQuestion(Long id, QuestionModel questionModel) {
		QuestionModel questionToUpdate = getQuestion(questionModel.getQuestion_id());
		questionToUpdate.setQuestion(questionModel.getQuestion());
		questionToUpdate.setCorrectAnswer(questionModel.getCorrectAnswer());
		questionToUpdate.setFirstWrongAnswer(questionModel.getFirstWrongAnswer());
		questionToUpdate.setSecondWrongAnswer(questionModel.getSecondWrongAnswer());
		questionToUpdate.setCategory(questionModel.getCategory());
		questionToUpdate.setSubCategory(questionModel.getSubCategory());
		System.out.println("Updateing questionmodel in database. From: " + questionModel.toString() + " to " + questionToUpdate.toString());
		questionRepository.saveAndFlush(questionToUpdate);
	}

	@Override
	public QuestionModel deleteQuestion(Long id) {
		QuestionModel questionModel = getQuestion(id);
		questionRepository.delete(id);
		System.out.println("Deleting questionmodel in database: " + questionModel.toString());
		return questionModel;
	}

	@Override
	public List<QuestionModel> getAllQuestions() {
		List<QuestionModel> allQuestionsInDB = questionRepository.findAll();
		System.out.println("Getting all questions from database: ");
		
		return allQuestionsInDB;
	}


}
