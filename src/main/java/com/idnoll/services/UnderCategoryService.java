package com.idnoll.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idnoll.models.UnderCategoryModel;
import com.idnoll.repositories.UnderCategoryRepository;

@Service
public class UnderCategoryService {

	

	@Autowired
	private UnderCategoryRepository undercategoryRepository;

	public UnderCategoryService() {

	}

	public List<UnderCategoryModel> findAllUndercategory() {
		List<UnderCategoryModel> listOfAllUnderCategoryModels = undercategoryRepository.findAll();

		System.out.println("Getting all undercategories from database: ");
		
		return listOfAllUnderCategoryModels;

	}

	public UnderCategoryModel findOne(Long underCategoryId) {
		UnderCategoryModel underCategoryModel = undercategoryRepository.findOne(underCategoryId);
		System.out.println("Getting underCategory from database: " + underCategoryModel.toString());
		return underCategoryModel;

	}

	public UnderCategoryModel createUnderCategory(UnderCategoryModel undercategoryModel) {
		System.out.println("Saving underCategory to database: " + undercategoryModel.toString());
		return undercategoryRepository.save(undercategoryModel);
	}
	
	public void deleteUnderCategory(Long id){
		undercategoryRepository.delete(id);
		
	}

}