package com.idnoll.services;

import java.util.List;

import com.idnoll.models.UserModel;


public interface UserServiceInterface {

	
		
		public UserModel createUser(UserModel userModel);
		public UserModel getUser(Long user_id);
		public void updateUser(Long user_id, UserModel userModel);
		public UserModel deleteUser(Long user_id);
		public List<UserModel> getAllUsers();
		
	
}
