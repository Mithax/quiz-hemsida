package com.idnoll.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.idnoll.models.UserModel;
import com.idnoll.repositories.UserRepository;

@Service
public class UserService implements UserServiceInterface{

	@Autowired
	private UserRepository userRepository;
		
	@Override
	public UserModel createUser(UserModel userModel){
		return userRepository.save(userModel);
	}
	
	@Override
	public UserModel getUser(Long user_id) {
		return userRepository.findOne(user_id);
	}

	@Override
	public void updateUser(Long user_id, UserModel userModel) {
		UserModel userToUpdate = getUser(userModel.getUser_id());
		userToUpdate.setFirstName(userModel.getFirstName());
		userToUpdate.setLastName(userModel.getLastName());
		userToUpdate.setUserName(userModel.getUserName());
		userToUpdate.setEmail(userModel.getEmail());
		userToUpdate.setPassword(userModel.getPassword());
		userToUpdate.setIsAdmin(userModel.getIsAdmin());
		userRepository.saveAndFlush(userToUpdate);
	}

	@Override
	public UserModel deleteUser(Long user_id) {
		UserModel userModel = getUser(user_id);
		userRepository.delete(user_id);
		return userModel;
	}
	
	@Override
	  public List<UserModel> getAllUsers() {
	    List<UserModel> result = userRepository.findAll();
	    return result;
	  }
	
	
	public List<UserModel> findAllUsers(){
		return userRepository.findAll();
	}


	public UserModel findOne(Long user_id) {
		return userRepository.findOne(user_id);
		
	}
	
	 // hämta specifik användare med USERNAME
    public UserModel findUserByUserName(String userName) {
    UserModel userModel = userRepository.findUserByUserName(userName);
    System.out.println("Called method 'findUserByUserName' with username '" +userModel.getUserName()+ "'");
    return userModel;
    }
    
    // hämta specifik användare med EMAIL
    public UserModel findUserByEmail(String email){
    	UserModel userModel = userRepository.findUserByEmail(email);
    	System.out.println("Called method 'findUserByEmail' with email '" +userModel.getEmail()+" ID: "+ userModel.getUser_id()+ "'");
    	return userModel;
    }
	
    // delete användar med ID
    public void deleteUserInDatabase(Long user_id) {
        userRepository.delete(user_id);
        System.out.println("User deleted with ID = " +user_id);
    }

 // uppdatera specifik användare med ID
    public void updateUserInDatabase(Long user_id, UserModel userModel) {

        UserModel userToUpdate = userRepository.getOne(user_id);
        userToUpdate.setUserName(userModel.getUserName());
        userToUpdate.setEmail(userModel.getEmail());
        userToUpdate.setPassword(userModel.getPassword());
        userRepository.saveAndFlush(userToUpdate);
        System.out.println("User with ID = " +user_id+ " has been updated by method 'updateUserInDatabase'");
    }

}
