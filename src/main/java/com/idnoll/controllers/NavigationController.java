package com.idnoll.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.idnoll.models.CategoryModel;
import com.idnoll.services.MainCategoryService;

@Controller
@RestController
public class NavigationController {
	
	

	@Autowired
	MainCategoryService categoryService;
	
	@RequestMapping(value={"/","index"}, method = RequestMethod.GET)
	public ModelAndView indexPage(HttpSession httpSession){
		List<CategoryModel> categories = new ArrayList<>();
		categories = categoryService.getAllCategories();
		httpSession.setAttribute("categories", categories);
		System.out.println("Get reguest sent to /index, returning Index.jsp view");
		return new ModelAndView("index");
	}

}
