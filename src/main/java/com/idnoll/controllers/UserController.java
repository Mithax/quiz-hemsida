package com.idnoll.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.idnoll.models.UserModel;
import com.idnoll.services.UserService;
import com.idnoll.services.UserServiceInterface;

@RestController
@Controller
@RequestMapping(value="")
@Configuration
@ComponentScan("com.idnoll.services")
public class UserController {
	
	
	
	@Autowired
	UserServiceInterface userServiceInterface;
	
	@Autowired 
	private UserService userService;
	
	
	
	@RequestMapping(value="/userRegistration", method = RequestMethod.GET)
	  public ModelAndView showRegPage() {
		System.out.println("Get request sent via GET to /userRegistration");
		ModelAndView maw = new ModelAndView("userRegistration");	
	    return maw;
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	  public ModelAndView logout(HttpSession session) {
		System.out.println("Get request sent to /logout");
		session.setAttribute("currentUser", null);
		ModelAndView maw = new ModelAndView("index");	
	    return maw;
	}
	
	
	@RequestMapping(value="/logInForm", method = RequestMethod.POST)
	  public ModelAndView handleLogIn(HttpServletRequest request) {
		System.out.println("Get request sent via POST to /logInForm");
		ModelAndView mav;
		String stringMessage;		
		String email = request.getParameter("inputEmail");
		UserModel currentUser = new UserModel();
		
		try{
		currentUser = userService.findUserByEmail(email);
		System.out.println("Getting all users´emails from database");
		}catch(Exception e){
			System.err.println("There are no emails in the database. -From inside handleLogIn().");
			currentUser = null;
		}
		if(currentUser == null){
			stringMessage = "Hittar ingen användare med angiven email";
			mav = new ModelAndView("userRegistration");
		}else{
			if(currentUser.getPassword().equals(request.getParameter("inputPassword"))){
				HttpSession session = request.getSession();
				mav = new ModelAndView("welcome");				
				session.setAttribute("currentUser", currentUser);
				stringMessage = "Success! Du är nu inloggad!";
				mav.addObject("stringMessage", stringMessage);	
				System.out.println("The user succeded to login with the correct password");
			}else{
				stringMessage = "Lösenord matcha inte användarens email";
				mav = new ModelAndView("userRegistration");
				System.err.println("The user failed to login. Fel password. -From inside handleLogIn");
			}
		}
		mav.addObject("stringMessage", stringMessage);
	    return mav;
	}
	
	@RequestMapping(value="/regForm", method = RequestMethod.POST)
	  public ModelAndView handleRegistration(HttpServletRequest request) {
		System.out.println("Get request sent via POST to /regForm");
		String stringMessage;
		ModelAndView mav;
		if(checkIfEmailNotYetRegistred(request.getParameter("inputEmail"))){			
			stringMessage = "Error. Email finns redan. Gör ett nytt försök att registrera dig!";
			mav = new ModelAndView("userRegistration");
			mav.addObject("stringMessage", stringMessage);
			System.err.println("Failed to register with an email that already exists: " + request.getParameter("inputEmail"));
			return mav;
		}
		
		String firstName = request.getParameter("inputFirstName");
		String lastName = request.getParameter("inputLastName");
		String userName = request.getParameter("inputUserName");
		String email = request.getParameter("inputEmail");			
		String password = request.getParameter("inputPassword");
		@SuppressWarnings("unused")
		String passwordVerification = request.getParameter("inputPasswordVerification");

		UserModel newUser = new UserModel();
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);
		newUser.setUserName(userName);		
		newUser.setEmail(email);	
		newUser.setPassword(password);
		userService.createUser(newUser);	
		
		HttpSession session = request.getSession();
		session.setAttribute("currentUser", newUser);
		mav = new ModelAndView("welcome");	
		stringMessage = "Success! Du är nu Registrerad!";
		mav.addObject("stringMessage", stringMessage);
		System.out.println("User succedded registrering a new user and is redirected to index with a welcome message");

	    return mav;
	}
	
	public boolean checkIfEmailNotYetRegistred(String email){
		boolean emailExist = false;
		List<UserModel> listOfUsers = new ArrayList<>();
		try {
			System.out.println("Getting all users from database and checking if the email they try to use existis already. -From inside checkIfEmailNotYetRegistred()");
			listOfUsers = userService.findAllUsers();
			for(UserModel emailsAlreadyRegistred : listOfUsers){
				if(emailsAlreadyRegistred.getEmail().equals(email)){
					emailExist = true;
				}
			}
		}catch(Exception e){
			e.getMessage();
			System.err.println("Failed use an existing email to register a user. Email used: " + email);			
		}
		return emailExist;
	}
	
}
