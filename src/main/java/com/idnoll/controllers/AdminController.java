package com.idnoll.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.http.MediaType;

import org.apache.log4j.Logger;
import com.idnoll.models.QuestionModel;
import com.idnoll.models.UnderCategoryModel;
import com.idnoll.services.AdminServiceInterface;
import com.idnoll.services.UnderCategoryService;

import com.idnoll.models.CategoryModel;
import com.idnoll.services.CategoryService;

@RestController
@Controller
@RequestMapping(value = "")
@Configuration
@ComponentScan("com.idnoll.services")
public class AdminController {

	

	@Autowired
	private AdminServiceInterface adminServiceInterface;

	@Autowired
	private UnderCategoryService undercategoryService;

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = "/addQuestion/{id}", method = RequestMethod.GET)
	public ModelAndView getQuestion(@PathVariable("id") String id) {

		System.out.println("Get reguest sent to /addQuestion/" + id + " , entering getQuestion method");

		Long underCategoryId = Long.parseLong(id);
		ModelAndView model;

		try {
			System.out.println("Getting undercategory from database");
			UnderCategoryModel underCategoryModel = undercategoryService.findOne(underCategoryId);

			System.out.println("Getting category from database");
			CategoryModel categoryModel = categoryService.findOne(underCategoryModel.getCategoryId());

			Map<String, Object> models = new HashMap<>();
			models.put("underCategoryModel", underCategoryModel);
			models.put("categoryModel", categoryModel);
			models.put("questionModel", new QuestionModel());

			model = new ModelAndView("addQuestion");
			model.addAllObjects(models);

		} catch (Exception e) {
			model = new ModelAndView("errorPage");
			model.addObject("errorMessage", "Kunde inte hämta fråga");
			System.err.println("Failed to get get categories from database, message: " + e.getMessage());
		}

		return model;

	}

	@RequestMapping(value = "/createQuestion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public QuestionModel createQuestion(@RequestBody QuestionModel questionModel) {
		System.out.println("Post reguest sent to /createQuestion/, entering createQuestion method");
		int counter = 0;
		counter = checkNumberOfQuestions(questionModel.getSubCategory());
		System.out.println("Getting the number of questions for current quiz: " + counter);
		if (counter < 15) {
			System.out.println("Number of questions is less then 15, returning to createquestionpage");
			return adminServiceInterface.createQuestion(questionModel);
		} else {
			System.out.println("All quiz questions created, redirecting to getQuizCreatedPage");
			getQuizCreatedPage();
		}
		return null;
	}

	private int checkNumberOfQuestions(Long id) {
		System.out.println("Entering checkNumberOfQuestions method with subcategory id: " + id);
		
		int numberOfQuestions = 0;
		List<QuestionModel> allQuestionsInDatabase = new ArrayList<>();
		try {
			allQuestionsInDatabase = adminServiceInterface.getAllQuestions();
			for (int i = 0; i < allQuestionsInDatabase.size(); i++) {
				if (allQuestionsInDatabase.get(i).getSubCategory() == id) {
					numberOfQuestions++;
				}
			}
		} catch (Exception e) {
			System.err.println("Could not get questions from database. Message: " + e.getMessage());
		}

		return numberOfQuestions;
	}

	@RequestMapping(value = "/editQuestion/{id}", method = RequestMethod.GET)
	public ModelAndView getQuestionToEdit(@PathVariable Long id) {
		System.out.println("Post reguest sent to /editQuestion/" + id + ", entering createQuestion method");

		ModelAndView model;
		try {
			QuestionModel questionModel = adminServiceInterface.getQuestion(id);

			model = new ModelAndView("editQuestion");
			model.addObject("questionModel", questionModel);
			System.out.println("Getting a question to be edited from database and returning in model: "
					+ questionModel.toString());
		} catch (Exception e) {

			model = new ModelAndView("errorPage");
			model.addObject("errorMessage", "Kunde inte hämta frågor");
			System.err.println("Failed to get question to edit from database, message: " + e.getMessage());
		}

		return model;
	}

	@RequestMapping(value = "/editQuestion/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ModelAndView editQuestion(@PathVariable Long id, @RequestBody QuestionModel questionModel) {
		System.out.println("Put reguest sent to /editQuestion/" + id + ", entering editQuestion method");
		ModelAndView model;
		try {
			model = new ModelAndView("listOfQuestions");
			questionModel.setQuestion_id(id);
			adminServiceInterface.updateQuestion(id, questionModel);
			System.out.println("Updating question in database: " + questionModel.toString());

		} catch (Exception e) {
			model = new ModelAndView("errorPage");
			System.err.println("Failed to update question in database, message: " + e.getMessage());
		}
		return model;
	}

	@RequestMapping(value = "/listOfQuestions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<QuestionModel> allQuestions() {
		try {
			List<QuestionModel> allQuestions = adminServiceInterface.getAllQuestions();
			System.out.println("Getting all questions from database. Total size: " + allQuestions.size());

			return allQuestions;
		} catch (Exception e) {
			System.err.println("Failed to get all questions from database: " + e.getMessage());
			return null;
		}

	}

	@RequestMapping(value = "/listOfQuestions", method = RequestMethod.GET)
	public ModelAndView listOfQuestions() {
		System.out.println("Get request sent to /listOfQuestions, entering listOfQuestions method");

		ModelAndView model = new ModelAndView("listOfQuestions");
		Map<String, Object> models = new HashMap<>();
		List<QuestionModel> listOfQuestions = new ArrayList<>();
		List<UnderCategoryModel> listOfUnderCategories = new ArrayList<>();

		try {
			listOfUnderCategories = undercategoryService.findAllUndercategory();
			System.out.println("Getting all undercategories from database. Total size: " + listOfUnderCategories.size());

			listOfQuestions.addAll(allQuestions());
			models.put("listOfQuestions", listOfQuestions);
			models.put("listOfUnderCategories", listOfUnderCategories);

			model = new ModelAndView("listOfQuestions");
			model.addAllObjects(models);

		} catch (Exception e) {

			model = new ModelAndView("errorPage");
			model.addObject("errorMessage", "Kunde inte hämta frågor");
			System.err.println("Failed to add questions to undercategory. Message: " + e.getMessage());
		}

		return model;

	}

	@RequestMapping(value = "/quizCreated", method = RequestMethod.GET)
	public ModelAndView getQuizCreatedPage() {
		System.out.println("Get request sent to /quizCreated, Returning quizCreated.jsp");

		ModelAndView model = new ModelAndView("quizCreated");
		return model;
	}

}
