package com.idnoll.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.idnoll.models.UserModel;
import com.idnoll.services.UserService;

@RestController
@Controller
@RequestMapping(value = "")
@Configuration
public class MakeOrUnmakeAdmin {

	
	
	@Autowired
	UserService userService;


	@RequestMapping(value = "/AdminStatus", method = RequestMethod.GET)
	public ModelAndView showAddRemoveAdminPage() {
		System.out.println("Get reguest sent to /Adminstatus, entering showAddRemoveAdminPage method");

		ModelAndView mav;

		List<UserModel> listOfUsers = new ArrayList<UserModel>();

		try {

			listOfUsers = userService.findAllUsers();
			mav = new ModelAndView("SearchForUsersToMakeAdmin");
			mav.addObject("listOfUsers", listOfUsers);
			System.out.println("Getting all users from Database and storing in ArrayList listOfUsers with size"
					+ listOfUsers.size());

		} catch (Exception e) {

			System.err.println("Failed to get all users from database. Message: " + e.getMessage());
			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Kunde inte hämta användare");

		}
		return mav;
	}

	@RequestMapping(value = "/AdminStatus", method = RequestMethod.POST)
	public ModelAndView changeAdminStatus(HttpServletRequest request, HttpSession session) {

		System.out.println("Post reguest sent to /Adminstatus, entering changeAdminStatus method");

		ModelAndView mav = new ModelAndView("MakeAdminSucess");

		UserModel userToBeChanged = new UserModel();

		try {
			userToBeChanged = userService.findOne(Long.valueOf(request.getParameter("userID")).longValue());
			userToBeChanged.setIsAdmin(true);
			userService.updateUser(userToBeChanged.getUser_id(), userToBeChanged);
			
			session.setAttribute("currentUser", userToBeChanged);
			
			mav = new ModelAndView("MakeAdminSucess");
			mav.addObject("user", userToBeChanged);

			System.out.println("Updating " + userToBeChanged.toString() + " in database");
		} catch (Exception e) {

			userToBeChanged = null;

			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Could not update user in database");

			System.err.println("Failed to update userstatus to update. Message: " + e.getMessage());

		}

		return mav;
	}

}
