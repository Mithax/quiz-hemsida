package com.idnoll.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.idnoll.models.QuestionModel;
import com.idnoll.services.AdminService;

@RestController
@Controller
@RequestMapping(value = "")
@Configuration
public class QuizController {

	

	public List<QuestionModel> questions = new ArrayList<>();
	public List<QuestionModel> questionsToLoop = new ArrayList<>(); 

	
	@Autowired
	private AdminService adminService;

	@RequestMapping(value = "/quizPage/{undercategoryId}", method = RequestMethod.GET)
	public ModelAndView quizPage(@PathVariable("undercategoryId") String undercategoryId) {
		Long subCategory = Long.parseLong(undercategoryId);
		questions.clear();
		questionsToLoop.clear();
		questionsToLoop = adminService.getAllQuestions();
		for (int i = 0; i < questionsToLoop.size(); i++) {
			if (questionsToLoop.get(i).getSubCategory().equals(subCategory)) {
				questions.add(questionsToLoop.get(i));
			}
		}
		for (int i = 0; i < questions.size(); i++) {
			questions.get(i).setRandomPosition();
		}
		
		System.out.println("Get reguest sent to /quizPage, entering quizPage method");
		ModelAndView model = new ModelAndView("quizPage");
		model.addObject("questions", questions);
		return model;
	}

	@RequestMapping(value = "/random", method = RequestMethod.GET)
	public ModelAndView randomQuizPage() {
		System.out.println("Get reguest sent to /random, entering randomQuiz method");

		ModelAndView mav;
		int dbSize;
		List<Long> randomQuestionsNumber = new ArrayList<>();
		Random random = new Random();

		questions.clear();

		try {
			for (int i = 0; i < adminService.getAllQuestions().size(); i++) {
				randomQuestionsNumber.add(adminService.getAllQuestions().get(i).getQuestion_id());
			}
			
			dbSize = adminService.getAllQuestions().size();
			System.out.println("Getting total size of question database " + dbSize);

		} catch (Exception e) {

			System.err.println(("Failed to get all database items. Message: " + e.getMessage()));
			dbSize = 0;

			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Kunde inte hämta frågor ifrån databasen");

			return mav;

		}

		if (dbSize >= 30) {
			System.out.println("Setting 15 random Questions in list forwared to quizPage");

			while (questions.size() < 15) {
				questions.add(adminService.getQuestion(randomQuestionsNumber.get(random.nextInt(dbSize))));
				questions = checkListForDuplicates(questions);
			}
			
			for (int i = 0; i < questions.size(); i++) {
				questions.get(i).setRandomPosition();
			}

			mav = new ModelAndView("quizPage");
			mav.addObject("questions", questions);

		} else {

			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Det finns inte tillräckligt med sparade frågor");

			System.err.println("To few questions in database");
		}

		return mav;
	}

	public List<QuestionModel> checkListForDuplicates(List<QuestionModel> questions) {
		System.out.println("Entering checkForDuplicate method with question listsize: " + questions.size());

		Set<QuestionModel> uniqueQuestions = new HashSet<QuestionModel>(questions);

		if (uniqueQuestions.size() < questions.size()) {
			System.out.println("Last added question already exists");

			questions.clear();
			questions.addAll(uniqueQuestions);
		}

		return questions;
	}

	@RequestMapping(value = "/quizPage", method = RequestMethod.POST)
	public ModelAndView showTotalCorrectAnswers(HttpServletRequest request) {
		System.out.println("Post reguest sent to /quizPage, entering showTotalCorrectAnswers method");

		List<String> answers = new ArrayList<>();
		ModelAndView mav = new ModelAndView("numberOfCorrectAnswersInQuiz");

		for (int i = 0; i < questions.size(); i++) {
			answers.add(request.getParameter("answer" + i));
		}

		int correctAnswers = checkNumberOfRightAnswers(answers);

		Integer[] correctAndTotalAnswers = { correctAnswers, questions.size() };

		mav.addObject("correctAndTotalAnswers", correctAndTotalAnswers);

		if (questions.size() != 15) {
			System.err.println("Total questions size is not 15 but : " + questions.size());
		} else {
			System.out.println("Returning Integer array with corect answers: " + correctAnswers + " and total answers: "
					+ questions.size());
		}

		return mav;

	}

	public Integer checkNumberOfRightAnswers(List<String> userAnswers) {
		System.out.println("Entering checkNumberOfRightAnswers method with list of size: " + userAnswers.size());

		int correctAnswerCounter = 0;

		for (int i = 0; i < userAnswers.size(); i++) {

			if (questions.get(i).getCorrectAnswer().equals(userAnswers.get(i))) {				
				correctAnswerCounter++;
			}
		}
		System.out.println("Returning correctAnswerCounter: " + correctAnswerCounter);
		return correctAnswerCounter;

	}
}
