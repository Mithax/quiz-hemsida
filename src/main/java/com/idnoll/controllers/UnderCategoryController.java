package com.idnoll.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.idnoll.services.UnderCategoryService;
import com.idnoll.models.*;

@RestController
@Controller
@RequestMapping(value = "")
@Configuration
public class UnderCategoryController {


	@Autowired
	private UnderCategoryService undercategoryService;

	@RequestMapping(value = "/undercategory/{categoryId}", method = RequestMethod.GET)
	public ModelAndView listAllUndercategories(@PathVariable("categoryId") String categoryId) {
		Long id = Long.parseLong(categoryId);
		System.out.println("Get reguest sent to /undercategory, entering listAllUndercategories method");
		
		ModelAndView mav;

		try {
			
			List<UnderCategoryModel> undercategories = new ArrayList<>();
			List<UnderCategoryModel> undercategoriesToLoop = new ArrayList<>();
			undercategoriesToLoop = undercategoryService.findAllUndercategory();
			for (int i = 0; i < undercategoriesToLoop.size(); i++) {
				if (undercategoriesToLoop.get(i).getCategoryId() == id) {
					undercategories.add(undercategoriesToLoop.get(i));
				}
			}
			mav = new ModelAndView("undercategory");
			mav.addObject("undercategories", undercategories);

			System.out.println(("Getting all undercategories from database for List (undercategories) with size: "
					+ undercategories.size()));

		} catch (Exception e) {
			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Kunde inte hÃ¤mta underkategorier");

			System.err.println(("Failed to get all categories from database. Message: " + e.getMessage()));
		}

		return mav;
	}

	@RequestMapping(value = "/deleteQuiz", method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<UnderCategoryModel> allUnderCategories() {
		try {
			System.out.println("Getting list with all undercategories");
			return undercategoryService.findAllUndercategory();
		} catch (Exception e) {
			System.err.println("Failed to get all undercategories. Message: " + e.getMessage());
			return null;
		}
	}

	@RequestMapping(value = "/deleteQuiz", method = RequestMethod.GET)
	public ModelAndView getAllUndercategories() {
		ModelAndView mav;
		List<UnderCategoryModel> underCategories = new ArrayList<>();

		try {
			underCategories.addAll(allUnderCategories());

			mav = new ModelAndView("deleteUnderCategory");
			mav.addObject("underCategories", underCategories);

		} catch (Exception e) {

			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Kunde inte hÃ¤mta underkategorier");

			System.err.println("Failed to get all undercategories. Message: " + e.getMessage());
		}

		return mav;
	}

	@RequestMapping(value = "/deleteQuiz/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView deleteUnderCategory(@PathVariable Long id) {
		ModelAndView mav;
		try {
			mav = new ModelAndView("redirect:/deleteQuiz");
			undercategoryService.deleteUnderCategory(id);
			System.out.println(("Deleting undercategory with id: " + id));
			return mav;
		} catch (Exception e) {
			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Kunde inte ta bort frågan från databasen");
			System.err.println("Could not delete undercategory. Message: " + e.getMessage());
			return mav;
		}
	}
}