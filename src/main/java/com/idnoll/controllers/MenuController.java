package com.idnoll.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.idnoll.models.CategoryModel;
import com.idnoll.services.CategoryService;

@RestController
@Controller
@RequestMapping(value = "")
@Configuration
public class MenuController {

	

	@Autowired
	private CategoryService menuService;

	@RequestMapping("/menu")
	public String menu() {
		System.out.println("Request to /menu sent. Returning menu.jsp");
		return "menu";
	}

	@RequestMapping("/undercategoryMenu")
	public String undermenu() {
		System.out.println("Request to /underCategoryMenu sent. Returning undercategory_menu.jsp");
		return "undercategory menu";
	}

	@RequestMapping("/menus")
	public ModelAndView menus() {
		System.out.println("Request to /menus sent");

		ModelAndView mav;
		List<CategoryModel> listOfAllMenuItems = new ArrayList<>();

		try {

			listOfAllMenuItems = menuService.findAllMenuCategory();

			mav = new ModelAndView("menus");
			mav.addObject("menus", listOfAllMenuItems);

			System.out.println("Getting list of all categories and putting in listOfAllMenuItems ArrayList with size"
					+ listOfAllMenuItems.size());

		} catch (Exception e) {

			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Kunde inte hämta menyn");

			System.err.println("Failed to get all categories from database. Message: " + e.getMessage());

		}

		return mav;
	}

}
