package com.idnoll.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.idnoll.models.CategoryModel;
import com.idnoll.models.UnderCategoryModel;
import com.idnoll.services.CategoryService;
import com.idnoll.services.MainCategoryService;
import com.idnoll.services.UnderCategoryService;

@RestController
@RequestMapping(value = "")
@Configuration
@ComponentScan("com.idnoll.services")
public class CategoryController {


	@Autowired
	CategoryService categoryService;
	

	@Autowired
	private UnderCategoryService undercategoryService;

	@Autowired
	private MainCategoryService mainCategoryService;

	@RequestMapping(value = "/createMainCategory", method = RequestMethod.GET)
	public ModelAndView getMainCategory() {
		System.out.println(
				"Get reguest sent to /createMainCategory, Returning createMainCategory.jsp with a new CategoryModel");
		ModelAndView model = new ModelAndView("createMainCategory");
		model.addObject("categoryModel", new CategoryModel());
		return model;
	}

	@RequestMapping(value = "/createNewMainCategory", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView createMainCategory(HttpServletRequest request, HttpSession session) {
		ModelAndView mav;
		
		try {
			List<CategoryModel> categoryModels = mainCategoryService.getAllCategories();
			CategoryModel categoryModel = new CategoryModel();
			categoryModel.setCategoryName(request.getParameter("name"));
			for (int i = 0; i < categoryModels.size(); i++) {
				if (categoryModels.get(i).getCategoryName().equals(categoryModel.getCategoryName())) {
					mav = new ModelAndView("errorPage");
					mav.addObject("errorMessage", "Det finns redan en kategori med det namnet i databasen");
					return mav;
				}
			}
			mainCategoryService.createCategory(categoryModel);
			mav = new ModelAndView("redirect:/createCategory");
			
			session.setAttribute("categories", categoryModels);
			System.out.println("Adding category to database: " + categoryModel.toString());
			
		} catch (Exception e) {
			System.out.println("Failed to add category to database. Message: " + e.getMessage());
			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Kunde inte spara kategorin till databasen");
		}
		
		return mav;
	}

	@RequestMapping(value = "/createCategory", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<CategoryModel> allMainCategories() {
		System.out.println("Entering allMainCategories method");
		List<CategoryModel> allMainCategories;
		try {
			allMainCategories = mainCategoryService.getAllCategories();
			System.out.println("Getting all categories from database with size: " + allMainCategories.size());

		} catch (Exception e) {
			allMainCategories = null;
			System.out.println("Failed to get all categories from database. Message: " + e.getMessage());
		}
		return allMainCategories;
	}

	@RequestMapping(value = "/createCategory", method = RequestMethod.GET)
	public ModelAndView getCategory() {
		System.out.println("Get reguest sent to /createCategory, entering getCategory method");

		Map<String, Object> modelMap = new HashMap<String, Object>();

		ModelAndView mav;
		List<CategoryModel> categoryModels = new ArrayList<>();
		try {
			categoryModels.addAll(allMainCategories());

			modelMap.put("categoryModels", categoryModels);
			modelMap.put("categoryModel", new CategoryModel());
			modelMap.put("underCategoryModel", new UnderCategoryModel());

			mav = new ModelAndView("createCategory");
			mav.addAllObjects(modelMap);

			System.out.println("Returning view with List categoryModels, new CategoryModel and new UnderCategoryModel");

		} catch (Exception e) {
			mav = new ModelAndView("errorPage");
			mav.addObject("errorMessage", "Kunde ej hämta kategorier");
			System.err.println("Could not return find categories. Message: " + e.getMessage());
		}

		return mav;
	}

	@RequestMapping(value = "/createCategory", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public UnderCategoryModel createCategory(@RequestBody UnderCategoryModel underCategoryModel) {
		try {
			System.out.println("Saving category to database: " + underCategoryModel.toString());
			return undercategoryService.createUnderCategory(underCategoryModel);

		} catch (Exception e) {
			System.err.println("Could not save undercategory to database. Message: " + e.getMessage());
			return null;
		}
	}

}
