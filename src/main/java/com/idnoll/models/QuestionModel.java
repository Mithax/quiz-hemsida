
package com.idnoll.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;

@SuppressWarnings("serial")
@Entity
@Table(name = "questionmodel")
public class QuestionModel implements Serializable {

	@Id
	@GeneratedValue
	private Long question_id;

	@ForeignKey(name="undercategoryId")
	private Long subCategory;
	
	private String category;

	private String question;
	private String correctAnswer;
	private String firstWrongAnswer;
	private String secondWrongAnswer;


	//@JoinColumn(name = "menuModel")
	//private UnderCategoryModel underCategoryModel;

	@Transient
	private String[] randomPosition;

	public QuestionModel() {
	}

	public QuestionModel(String question, String correctAnswer, String firstWrongAnswer, String secondWrongAnswer,
			String category, Long subCategory) {
		this.question = question;
		this.correctAnswer = correctAnswer;
		this.firstWrongAnswer = firstWrongAnswer;
		this.secondWrongAnswer = secondWrongAnswer;
		this.category = category;
		this.subCategory = subCategory;
	}


	public String[] getShuffledAnswers() {
		List<String> randomAnswers = new ArrayList<>();

		randomAnswers.add(getCorrectAnswer());
		randomAnswers.add(getFirstWrongAnswer());
		randomAnswers.add(getSecondWrongAnswer());

		Collections.shuffle(randomAnswers);
		String[] randomAnswersArray = { randomAnswers.get(0), randomAnswers.get(1), randomAnswers.get(2) };
		return randomAnswersArray;
	}


	@Override
	public int hashCode() {
		return question_id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof QuestionModel))
			return false;

		QuestionModel questionModel = (QuestionModel) obj;
		return questionModel.question_id.equals(question_id);
	}
	
	
	public void setQuestion_id(Long question_id) {
		this.question_id = question_id;
	}

	public Long getQuestion_id() {
		return question_id;
	}


	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public String getFirstWrongAnswer() {
		return firstWrongAnswer;
	}

	public void setFirstWrongAnswer(String firstWrongAnswer) {
		this.firstWrongAnswer = firstWrongAnswer;
	}

	public String getSecondWrongAnswer() {
		return secondWrongAnswer;
	}

	public void setSecondWrongAnswer(String secondWrongAnswer) {
		this.secondWrongAnswer = secondWrongAnswer;
	}

	public Long getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(Long subCategory) {
		this.subCategory = subCategory;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String[] getRandomPosition() {
		return randomPosition;
	}

	public void setRandomPosition() {
		randomPosition = getShuffledAnswers();
	}

	@Override
	public String toString() {
		return "QuestionModel [question_id=" + question_id + ", subCategory=" + subCategory + ", category=" + category
				+ ", question=" + question + ", correctAnswer=" + correctAnswer + ", firstWrongAnswer="
				+ firstWrongAnswer + ", secondWrongAnswer=" + secondWrongAnswer + "]";
	}

	/*public UnderCategoryModel getUnderCategoryModel() {
		return underCategoryModel;
	}

	public void setUndercategoryModel(UnderCategoryModel underCategoryModel) {
		this.underCategoryModel = underCategoryModel;
	}*/

	
}
