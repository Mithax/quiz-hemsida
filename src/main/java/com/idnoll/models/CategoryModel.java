package com.idnoll.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "categories")
public class CategoryModel implements Serializable {

	@Id
	@GeneratedValue
	private Long categoryId;

	private String categoryName;

	

	public CategoryModel() {
	}



	public Long getCategoryId() {
		return categoryId;
	}

	
	
	public String getCategoryName() {
		return categoryName;
	}



	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}



	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "CategoryModel [categoryId=" + categoryId + ", categoryName=" + categoryName + "]";
	}
}
