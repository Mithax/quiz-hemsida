package com.idnoll.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@SuppressWarnings("serial")
@Entity

@Table(name = "undercategories")
public class UnderCategoryModel implements Serializable {

	@Id
	@GeneratedValue
	private Long undercategoryId;

	private String undercategoryName;
	  
	@ForeignKey(name = "categoryId")
	private Long categoryId;
	 
	public UnderCategoryModel() {

	}

	public Long getUndercategoryId() {
		return undercategoryId;
	}

	public void setUndercategoryId(Long undercategoryId) {
		this.undercategoryId = undercategoryId;

	}

	public String getUndercategoryName() {
		return undercategoryName;
	}

	public void setUndercategoryName(String undercategoryName) {
		this.undercategoryName = undercategoryName;

	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;

	}

	@Override
	public String toString() {
		return "UnderCategoryModel [undercategoryId=" + undercategoryId + ", undercategoryName=" + undercategoryName
				+ ", categoryId=" + categoryId + "]";
	}
	
}

