package com.idnoll.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="users")
public class UserModel implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long user_id;
	private String userName;
	private String email;
	private String password;
	private boolean isAdmin;	
	private String firstName;
	private String lastName;
	
	public UserModel() {
		
	}

	public UserModel(Long user_id, String userName, String email, String password, boolean isAdmin, String firstName,
			String lastName) {
		super();
		this.user_id = user_id;
		this.userName = userName;
		this.email = email;
		this.password = password;
		this.isAdmin = isAdmin;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "UserModel [user_id=" + user_id + ", userName=" + userName + ", email=" + email + ", password="
				+ password + ", isAdmin=" + isAdmin + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

}
