package com.idnoll.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.idnoll.models.UnderCategoryModel;


public interface UnderCategoryRepository extends JpaRepository<UnderCategoryModel, Long>{

}
