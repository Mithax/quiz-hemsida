package com.idnoll.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.idnoll.models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {
	
	public UserModel findUserByUserName(String userName);
	public UserModel findUserByEmail(String email);
}
