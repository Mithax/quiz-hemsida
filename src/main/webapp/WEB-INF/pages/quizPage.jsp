<%@ include file="taglib.jsp" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/default.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/quizPage.css">

<title>Fr�gesport</title>

<script src="${pageContext.request.contextPath}/js/Timer.js"></script>

</head>
<body>
<jsp:include page="page-header.jsp"></jsp:include>
	<div class="pageclass container lesserWidth">
        <form method="POST" action="${pageContext.request.contextPath}/quizPage">

            <c:forEach items="${questions}" var="questionModel" varStatus="counter">
              <div class="quizSection container">
              <h2>Fr�ga ${counter.index + 1}</h2>
                <textarea class="form-control quizTA" rows="5" wrap="hard" readonly>${questionModel.question}</textarea>


                	<div class="clearBoth">	
                		<input type="radio" name="answer${counter.index}" id="first${counter.index}" value="${ questionModel.randomPosition[0]}" class="radio"/>
						<label for="first${counter.index}">${questionModel.randomPosition[0]}</label>
					</div>
					<div class="clearBoth">	
                		<input type="radio" name="answer${counter.index}" id="second${counter.index}" value="${ questionModel.randomPosition[1]}" class="radio"/>
						<label for="second${counter.index}">${questionModel.randomPosition[1]}</label>
					</div>
					<div class="clearBoth">	
                		<input type="radio" name="answer${counter.index}" id="third${counter.index}" value="${ questionModel.randomPosition[2]}" class="radio"/>
						<label for="third${counter.index}">${questionModel.randomPosition[2]}</label>
					</div>
              </div>
            </c:forEach>
            <input class="btn btn-success" id="submit" type="submit" value="R�tta">
        </form>
    	<div class="timer">
    		<p>Tid kvar:</p>
      		<span id="remainingTime"></span>
    </div>
    </div>
<jsp:include page="page-footer.jsp"></jsp:include>
</body>
</html>
