<%@ include file="taglib.jsp" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/default.css">

<title>Underkategorier</title>

</head>

<body>
<jsp:include page="page-header.jsp"></jsp:include>

	<div>
	</div>
    
	<div class="">
    <div class="undercategories">
        <h3>V�lj underkategori p� din Quiz:</h3>
  	    <ul class="nav nav-tabs nav-stacked">
		  <c:forEach var="undercategories" items='${undercategories}'>
			  	 <li><a href="${pageContext.request.contextPath}/quizPage/${undercategories.undercategoryId}">${undercategories.undercategoryName}</a></li>		  
		  </c:forEach>
		</ul>      
    </div>
</div>
<jsp:include page="page-footer.jsp"></jsp:include>
</body>
</html>