<%@ include file="taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="${pageContext.request.contextPath}/js/GetMainCategories.js"></script>
<title>page header</title>

</head>
<body>
	<div class="jumbotron container-fluid">
		<c:choose>
			<c:when test="${currentUser == null}">
				<div class="login">
					<a class="btn btn-primary pull-right" href="${pageContext.request.contextPath}/userRegistration"><span
						class="glyphicon glyphicon-user"></span> Logga in</a>
				</div>
			</c:when>
			<c:otherwise>
				<div class="login">
					<a class="btn btn-primary pull-right"
						href="${pageContext.request.contextPath}/logout">Logga ut
						${currentUser.userName}</a>

				</div>
			</c:otherwise>
		</c:choose>
	</div>

	<div class="container">
		<div id="navigationList" class="nav-tabs">
			<ul class="nav nav-tabs nav-justified">
				<li><a href="${pageContext.request.contextPath}/index">Startsida</a></li>
				<c:forEach var="mainCategory" items="${categories}">
					<li><a
						href="${pageContext.request.contextPath}/undercategory/${mainCategory.categoryId}"><c:out
								value="${mainCategory.categoryName}"></c:out></a></li>
				</c:forEach>
				<li><a href="${pageContext.request.contextPath}/random">Slumpade
						fr�gor</a></li>
				<c:if test="${currentUser.isAdmin}">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href=""><span id="settings"
							class="glyphicon glyphicon-cog"></span> Meny</a>
						<ul class="dropdown-menu">
							<li><a href="${pageContext.request.contextPath}/AdminStatus">Hantera
									admins</a></li>
							<li><a
								href="${pageContext.request.contextPath}/createCategory.html">Skapa</a></li>
							<li><a
								href="${pageContext.request.contextPath}/listOfQuestions.html">�ndra</a></li>
							<li><a
								href="${pageContext.request.contextPath}/deleteQuiz.html">Ta
									bort</a></li>
						</ul></li>
				</c:if>

			</ul>
		</div>
	</div>
	<br>
	<br>
</body>
</html>


