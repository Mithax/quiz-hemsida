<%@ include file="taglib.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/default.css">

<title>Fr�gesport</title>

<script src="${pageContext.request.contextPath}/js/ListOfQuestionsScript.js"></script>

</head>
<body>
  <jsp:include page="page-header.jsp"></jsp:include> 
   <div class="quizTableWidth">

			<table id="usertable" class="table table-hover table-striped">
				<thead>
					<tr>
						<th class="col-md-1">#</th>
						<th class="col-md-10">Fr�ga</th>
						<th class="col-md-10">Kategori</th>
						<th class="col-md-1">Underkategori</th>
						<th class="col-md-1">�ndra</th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach var="questionModel" items="${listOfQuestions}">
						<tr>
							<th scope="row">${questionModel.question_id}</th>
							<td>${ questionModel.question }</td>
							<td>${ questionModel.category }</td>
							<c:forEach var="underCategoryModel" items="${listOfUnderCategories}">
			                  	<c:if test="${ questionModel.subCategory == underCategoryModel.undercategoryId}">
			                  		<td><c:out value="${underCategoryModel.undercategoryName}"></c:out></td>
			                  	</c:if>
		                  	</c:forEach>							
							<td><a href="${pageContext.request.contextPath}/editQuestion/${questionModel.question_id}"><span class="glyphicon glyphicon-pencil"></span></a><br/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

	</div>
 	<jsp:include page="page-footer.jsp"></jsp:include>
</body>
</html>
