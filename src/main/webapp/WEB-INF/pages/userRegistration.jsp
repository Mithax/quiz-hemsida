<%@ include file="taglib.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<script src="${pageContext.request.contextPath}/js/LoginAndRegistrationScript.js"/></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/default.css">


<title>Login</title>
</head>

<body>
	<jsp:include page="page-header.jsp"></jsp:include>
	<div class="">
		<form id="logInForm" action="${pageContext.request.contextPath}/logInForm" method="POST">

			<div class="container undercategories">
				<div class="col-md-12">
						<h3>Logga in</h3>
					</div>
				<c:if test="${stringMessage != null}">
				<div class="col-md-12 pageclass">
					<div class="alert alert-danger"><p style="color:red;">${stringMessage}</p></div>
					</div>
				</c:if>
				</div>
				<div class="container undercategories">
				<label for="inputEmail" class="col-md-3 control-label">Email</label>
				<div class="col-md-9">
					<input name="inputEmail" type="email" class="form-control" id="inputEmail"
						placeholder="email">
				</div>
			</div>
			<div class="container undercategories">
				<label for="inputPassword" class="col-md-3 control-label">L�senord</label>
				<div class="col-md-9">
					<input name="inputPassword" type="password" class="form-control" id="inputPassword"
						placeholder="l�senord">
				</div>
			</div>
			<div class="container undercategories">
				<div class="col-md-12 pageclass">

					<button id="btnLogIn" class="btn btn-primary">Login</button>
				</div>
			</div>
			<div class="container undercategories" id="reg">
				<div class="col-md--12 pageclass">
					<h3>�nnu inte medlem? Registrera dig h�r:</h3>
				</div>
				</div>
				<div class="container undercategories">
				<div class="col-md-12 pageclass">
					<button id="btnShowRegistering" type="button" class="btn btn-success"> Registrera dig</button>
				</div>
			</div>
		</form>



        <form id="regForm" action="${pageContext.request.contextPath}/regForm" method="POST">
            <div class="container undercategories">
            	<div class="col-md-12">
				<h3>Registrering:</h3>
				</div>
			</div>
			<div class="container undercategories">
				<label for="inputUserName" class="col-md-3 control-label">Anv�ndarnamn</label>
				<div class="col-md-9">
					<input name="inputUserName" type="text" class="form-control" id="inputUserName"
						placeholder="anv�ndare namn">
						</div>
			</div>
			<div class="container undercategories">
				<label for="inputFirstName" class="col-md-3 control-label">Namn</label>
				<div class="col-md-9">
					<input name="inputFirstName" type="text" class="form-control" id="inputFirstName"
						placeholder="namn">
					</div>
			</div>
			<div class="container undercategories">
				<label for="inputLastName" class="col-sm-3 control-label">Efternamn</label>
				<div class="col-sm-9">
					<input name="inputLastName" type="text" class="form-control" id="inputLastName"
						placeholder="efternamn">
				</div>
			</div>
            <div class="container undercategories">
            <label for="inputEmail" class="col-md-3 control-label">Email</label>
				<div class="col-md-9">
					<input name="inputEmail" type="email" class="form-control" id="inputEmailR"
						placeholder="email">
				</div>
			</div>
			<div class="container undercategories">
				<label for="inputPassword" class="col-md-3 control-label">L�senord</label>
				<div class="col-md-9">
					<input name="inputPassword" type="password" class="form-control" id="inputPasswordR"
						placeholder="l�senord">
				</div>
			</div>
			<div class="container undercategories">
				<label for="inputPasswordVerification" class="col-md-3 control-label">L�senord (igen)</label>
				<div class="col-md-9">
					<input name="inputPasswordVerification" type="password" class="form-control" id="inputPasswordVerification"
						placeholder="l�senord verifiering">
				</div>
			</div>
			<div class="container undercategories">
				<div class="col-md-12 pageclass">
					<button id="btnAddUser" type="button" class="btn btn-success">Registrera</button>
				</div>
			</div>
		</form>
       </div>
	<jsp:include page="page-footer.jsp"></jsp:include>
</body>
</html>
