<%@ include file="taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/default.css">

<script
	src="${pageContext.request.contextPath}/js/SearchForUsersToMakeAdminScripts.js"></script>

<title>Ge adminr�ttigheter</title>

</head>
<body>
	<jsp:include page="page-header.jsp"></jsp:include>

	<div class="lesserWidth">

		<div class="form-group pull-right">
			<input id="search" type="text" class="form-control" placeholder="S�k anv�ndare">
		</div>
		<form id="selectUserForm" method="post">
			<table id="usertable" class="table table-hover table-striped">
				<thead>
					<tr>
						<th>#</th>
						<th class="col-md-2">Anv�ndarnamn</th>
						<th class="col-md-2">F�rnamn</th>
						<th class="col-md-2">Efternamn</th>
						<th class="col-md-4">Email</th>
						<th class="col-md-1">Admin</th>
						<th class="col-md-1">�ndra</th>
					</tr>
				</thead>
				<tbody id="tablebody">
					<c:forEach items="${listOfUsers}" var="user">
						<tr>
							<th scope="row">${user.user_id}</th>
							<td>${ user.userName }</td>
							<td>${ user.firstName }</td>
							<td>${ user.lastName }</td>
							<td>${ user.email }</td>
							<c:choose>
								<c:when test="${user.isAdmin == true}">
									<td><span class="glyphicon glyphicon-ok"></span></td>
								</c:when>
								<c:otherwise>
									<td><span class="glyphicon glyphicon-remove"></span></td>
								</c:otherwise>
							</c:choose>
							<td><input type="radio" name="userID" value="${user.user_id}"></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
				<div id="noSelectedUserAlert" class="alert alert-danger">
				  <strong>Ingen anv�ndare vald</strong>
				</div>
					<button onclick="submitForm()" type="button" class="btn btn-primary pull-right">�ndra</button>
		</form>
	</div>
	<jsp:include page="page-footer.jsp"></jsp:include>
</body>
</html>
