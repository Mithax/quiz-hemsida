<%@ include file="taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/default.css">

<title>Fr�gesport</title>

<script
	src="${pageContext.request.contextPath}/js/EditQuestionScript.js"></script>
	<script type="text/javascript"> var context = "${pageContext.request.contextPath}"</script>

</head>
<body>
	<jsp:include page="page-header.jsp"></jsp:include>

	<div id="editQuestion" class="pageclass">

		<div class="container">
			<form:form id="editQuestionForm" method="PUT"
				action="${pageContext.request.contextPath}/editQuestion/${questionModel.question_id}.json"
				commandName="questionModel">
				<div class="container">
					<div class="col-md-12">
						<h3>Ąndra fr�ga:</h3>
						<form:input type="hidden" path="category"
							value="${questionModel.category}"></form:input>
						<form:input type="hidden" path="subCategory"
							value="${questionModel.subCategory}"></form:input>
					</div>
				</div>
				<div class="container">
					<div class="col-md-1"></div>
					<label for=question class="col-md-2 control-label">Skriv in
						fr�ga</label>
					<div class="col-md-6">
						<form:textarea rows="6" path="question" class="form-control"
							id="question"></form:textarea>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="container">
					<div class="col-md-1"></div>
					<label for=correctAnswer class="col-md-2 control-label">R�tt
						Svar</label>
					<div class="col-md-6">
						<form:input path="correctAnswer" class="form-control"
							id="correctAnswer"></form:input>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="container">
					<div class="col-md-1"></div>
					<label for=firstWrongAnswer class="col-md-2 control-label">Fel
						svar 1</label>
					<div class="col-md-6">
						<form:input path="firstWrongAnswer" class="form-control"
							id="firstWrongAnswer"></form:input>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="container">
					<div class="col-md-1"></div>
					<label for=secondWrongAnswer class="col-md-2 control-label">Fel
						svar 2</label>
					<div class="col-md-6">
						<form:input path="secondWrongAnswer" class="form-control"
							id="secondWrongAnswer"></form:input>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="container">
					<div class="col-md-5"></div>
					<div class="col-md-2">
						<button class="btn btn-primary" type="submit" value="Submit"
							id="edit">Spara</button>
					</div>
					<div id="counter" class="col-md-2"></div>
					<div class="col-md-3"></div>
				</div>

			</form:form>
		</div>
	</div>
	<jsp:include page="page-footer.jsp"></jsp:include>
</body>
</html>
