<%@ include file="taglib.jsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/css/default.css">

<title>Fr�gesport</title>

<script src="${pageContext.request.contextPath}/js/CreateCategoryScript.js"></script>
<script type="text/javascript"> var context = "${pageContext.request.contextPath}"</script>


</head>
<body>
	<jsp:include page="page-header.jsp"></jsp:include>

	<div id="addQuestion" class="pageclass">

		<div id="divpopup" class="container">
			<form id="createMainCategoryForm"
				action="${pageContext.request.contextPath}/createNewMainCategory"
				commandName="categoryModel" method="get">
				<div class="vAlignMid">
				<div><h3>Namn p� kategorin:</h3></div>
					<div class="col-md-12">
						<input name="name" class="form-control"></input>
					</div>
				</div>

				<div class="col-md-6">
					<button id="popupbtn" type="submit"
						class="btn btn-primary marginTop">Spara</button>
				</div>
				<div class="col-md-6">
					<button id="close" type="button" class="btn btn-danger marginTop">
						<span id="close" class="glyphicon glyphicon-remove"></span>Avbryt
					</button>

				</div>
			</form>

		</div>
		<div class="container">
			<form:form id="newCategoryForm"
				action="${pageContext.request.contextPath}/createCategory.json"
				commandName="underCategoryModel">
				<div class="container">
					<div class="col-md-12">
						<h3>Skapa ny quiz</h3>
					</div>
				</div>
				<div class="container">
					<div class="col-md-1"></div>
					<label for="category" class="col-md-2 control-label">V�lj
						kategori</label>
					<div class="col-md-5">
						<form:select path="categoryId" class="form-control">
							<c:forEach var="mainCategory" items="${categoryModels}">
								<option value="${mainCategory.categoryId}" selected="true">${mainCategory.categoryName}</option>
							</c:forEach>
						</form:select>
					</div>
					<div class="col-md-1">
						<button id="loader" type="button" class="btn btn-primary">Ny
							huvudkategori</button>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="container">
					<div class="col-md-1"></div>
					<label for=underCategoryName class="col-md-2 control-label">Underkategori</label>
					<div class="col-md-6">
						<form:input path="undercategoryName" class="form-control"
							value="${underCategoryModel.undercategoryName}"></form:input>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="container">
					<div class="col-md-5"></div>
					<div class="col-md-2">
						<button id="save" type="submit" class="btn btn-primary">Spara
							underkategori</button>
					</div>
					<div class="col-md-5"></div>
				</div>
			</form:form>
		</div>
	</div>
	<jsp:include page="page-footer.jsp"></jsp:include>
</body>
</html>
