$(document).ready(function() {

	var listOfCategories;

	getCategories();

	function getCategories() {

		$.ajax({
			url: '${pageContext.request.contextPath}/page-header',
			type : "GET",
			contentType : 'application/json; charset=utf-8',
			dataType: 'json',
	
			success: function(data, textStatus, jqXHR){
				listOfCategories = data;
				populateTabs();
			}
	
		});
	
	}

	function populateTabs(){
		var strings = '';
		for (var i = 0; i < listOfCategories.length; i++) {
			strings +=	'<li>' + listOfCategories[i].categoryName + '</li>';
		}

		$('.nav-tabs ').append(strings);
	};
});	