$(document).ready(function() {		
	$('#regForm').hide();
	
	
	$('#btnShowRegistering').click(function () {
		$("#regForm").toggle(300);
	});


	$('#btnLogIn').click(function () {
	    var userEmail = $('#inputEmail').val();
	    var userPassword = $('#inputPassword').val();
	    if(userEmail == "" || userPassword == "" || userEmail == null || userPassword == null){
	    	alert("Please fill in Email and Password");
	    }else{
	    	console.log("else")
	    	$('#loginB').hide();
		    $('#currentUserName').show();
		    $('#logInForm').submit();
	    }
	});

	
	$('#btnAddUser').click(function () {
		var errorMessage = "";
		var userFirstName = $('#inputFirstName').val();
	    var userLastName = $('#inputLastName').val();
	    var userName = $('#inputUserName').val();
	    var userEmailR = $('#inputEmailR').val();
	    var userPasswordR = $('#inputPasswordR').val();
	    var verifyPassword = $('#inputPasswordVerification').val();
	    if(userFirstName == ""){
			errorMessage += "user first name saknas\n";
		}
	    if(userLastName == ""){
			errorMessage += "user last name saknas\n";
		}
	    if(userName == ""){
			errorMessage += "user name saknas\n";
		}
		if(userEmailR == ""){
			errorMessage += "email saknas\n";
		}
		if(userPasswordR == ""){
			errorMessage += "password saknas\n";
		}
		if( verifyPassword == ""){
			errorMessage += "password verification saknas\n";
		}
		if(userPasswordR != verifyPassword){
			errorMessage += "password stämmer inte med password verification\n";
		}
		if(errorMessage.length > 0){
			alert(errorMessage);
		}else{
			 $('#regForm').submit();
			 console.log("Hej");
		}
	    
	});
});









