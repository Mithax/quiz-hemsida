$(document).ready(function() {

	$("#divpopup").hide();
	
	$("#loader").click(function(){
		$("#divpopup").toggle(300, "linear");
	});
	$("#popupbtn").click(function(){
		$("#divpopup").hide();
		location.reload();
	});
	$("#close").click(function(){
		$("#divpopup").hide();
		location.reload();
	});
	
    $("#newCategoryForm").submit(function(event) {
      var categoryId = $("#categoryId").val();
      var undercategoryName = $("#undercategoryName").val();
      var json = {"categoryId":categoryId,"undercategoryName":undercategoryName};

      $.ajax({
        url: $("#newCategoryForm").attr("action"),
        data: JSON.stringify(json),
        type : "POST",
        contentType : 'application/json; charset=utf-8',
        dataType : 'json',
        
        success: function(data){
        	if (data != null) {
        		var id = data["undercategoryId"];
            	window.location.href = context+"/addQuestion/"+id;
			} else {
				alert("Det finns redan en underkategori med den namnet");
			}
        	
        }
      });
      event.preventDefault();
    });
});
