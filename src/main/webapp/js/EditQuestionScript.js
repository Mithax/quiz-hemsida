$(document).ready(function() {

    $("#editQuestionForm").submit(function(event) {
      var question = $("#question").val();
      var correctAnswer = $("#correctAnswer").val();
      var firstWrongAnswer = $("#firstWrongAnswer").val();
      var secondWrongAnswer = $("#secondWrongAnswer").val();
      var category = $("#category").val();
      var subCategory = $("#subCategory").val();
      var json = {"question":question,"correctAnswer":correctAnswer,"firstWrongAnswer":firstWrongAnswer,
            "secondWrongAnswer":secondWrongAnswer,"category":category,"subCategory":subCategory};

      $.ajax({
        url: $("#editQuestionForm").attr("action"),
        data: JSON.stringify(json),
        type : "PUT",

        beforeSend: function(xhr) {
          xhr.setRequestHeader("Accept", "application/json");
          xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(data) {
        		window.location.href = context + "/listOfQuestions";
        }
      });
      event.preventDefault();
    });
  });
