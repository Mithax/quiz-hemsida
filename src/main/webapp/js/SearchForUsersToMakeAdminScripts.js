//Checks the radoibutton if user clicks anywhere in the row
$(function() {
	$('#usertable').delegate('tbody > tr', 'click', function() {
		$(this).find('input[name="userID"]').attr('checked', true);
	});
});

// Makes the searchfield update the table
$(document).ready(function() {
	$("#noSelectedUserAlert").hide();
	(function($) {

		$("#search").keyup(function() {

			var regExp = new RegExp($(this).val(), 'i');
			$("#tablebody tr").hide();
			$("#tablebody tr").filter(function() {
				return regExp.test($(this).text());
			}).show();

		});

	}(jQuery));
});

// Check if user is selected then submits form

function submitForm() {
	
	var userID = $('[name="userID"]');
	var checked = false;
	var form = $("#selectUserForm");

	for (var i = 0; i < userID.length; i++) {
		if (userID[i].checked) {
			checked = true;
		}
	}
	if (checked) {
		$("#noSelectedUserAlert").hide();
		form.submit();
	} else {
		$("#noSelectedUserAlert").show();
	}
}
