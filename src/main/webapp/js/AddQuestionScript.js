$(document).ready(function() {

	var numberOfQuestions = 0;
	
  $("#newQuestionForm").submit(function(event) {
    var question = $("#question").val();
    var correctAnswer = $("#correctAnswer").val();
    var firstWrongAnswer = $("#firstWrongAnswer").val();
    var secondWrongAnswer = $("#secondWrongAnswer").val();
    var category = $("#category").val();
    var subCategory = $("#subCategory").val();
    var json = {"question":question,"correctAnswer":correctAnswer,"firstWrongAnswer":firstWrongAnswer,
          "secondWrongAnswer":secondWrongAnswer,"category":category,"subCategory":subCategory};

    $.ajax({
      url: $("#newQuestionForm").attr("action"),
      data: JSON.stringify(json),
      type : "POST",
      contentType : 'application/json; charset=utf-8',
      dataType: 'json',
      
      success: function(data) {
    	  $("#newQuestionForm")[0].reset();
    	  numberOfQuestions++;
    	  var antal = "Skapade: ";
    	  var setNumberOfQuestions = '<p>' + antal  + numberOfQuestions + ' / 15' + '</p>'; 
    	  $("#counter").empty();
    	  $("#counter").append(setNumberOfQuestions);
    	  
    	  if (numberOfQuestions == 15) {
    		$("#save").hide();
			window.location.href = context + "/quizCreated.html";
		}
      }
    });
    event.preventDefault();
  });
});
