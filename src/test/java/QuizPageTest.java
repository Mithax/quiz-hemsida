import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.idnoll.initializers.Initializer;
import com.idnoll.initializers.WebConfiguration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes={Initializer.class, WebConfiguration.class}, loader=AnnotationConfigWebContextLoader.class)
public class QuizPageTest {
	
	@Autowired 
	WebApplicationContext wac;
    
    private MockMvc mockMvc;
	
	@Before
	public void setup(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}	
	
	@Test
	public void testIfQuizExists() throws Exception{ 
		String id = "221";
        this.mockMvc.perform(get("/quizPage/"+id)
        		.accept(MediaType.TEXT_HTML))
        		.andExpect(status().isOk())
        		.andExpect(model().attributeExists("questions"));           			
	}
}
