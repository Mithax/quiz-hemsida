import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.asm.Type;
import org.mockito.runners.MockitoJUnitRunner;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

import com.idnoll.controllers.AdminController;
import com.idnoll.controllers.UnderCategoryController;
import com.idnoll.models.QuestionModel;
import com.idnoll.services.AdminService;

@RunWith(MockitoJUnitRunner.class)
public class AdminControllerMockitoTest {

	@InjectMocks
	private AdminController adminController;
	
	@InjectMocks
	private UnderCategoryController undercategoryController;
	
	@Mock
	private AdminService adminService;
	
	private static final Long QUESTION_ID_1 = 1L;
    private static final Long SUBCATEGORY_1 = 1L;
    private static final String CATEGORY_1 = "Geografi";
    private static final String QUESTION_1 = "Vad ligger söder om Orust";
    private static final String CORRECT_ANSWER_1 = "Tjörn";
    private static final String FIRST_WRONG_ANSWER_1 = "Koster";
    private static final String SECOND_WRONG_ANSWER_1 = "Lysekil";
    
    private static final Long QUESTION_ID_2 = 2L;
    private static final Long SUBCATEGORY_2 = 2L;
    private static final String CATEGORY_2 = "Musik";
    private static final String QUESTION_2 = "Vilken not är högst i skalan c, d eller f";
    private static final String CORRECT_ANSWER_2 = "c";
    private static final String FIRST_WRONG_ANSWER_2 = "d";
    private static final String SECOND_WRONG_ANSWER_2 = "f";

    private static final QuestionModel question1 = new QuestionModel(CATEGORY_1,QUESTION_1,CORRECT_ANSWER_1,FIRST_WRONG_ANSWER_1,SECOND_WRONG_ANSWER_1,SUBCATEGORY_1);
    private static final QuestionModel question2 = new QuestionModel(CATEGORY_2,QUESTION_2,CORRECT_ANSWER_2,FIRST_WRONG_ANSWER_2,SECOND_WRONG_ANSWER_2,SUBCATEGORY_2);
    
	
    @Test
    public void testEditQuestion() {
       adminController.editQuestion(SUBCATEGORY_2, question1);
       verify(adminService).updateQuestion(SUBCATEGORY_2, question1);
    }

    @Test
    public void testCreateQuestion(){
    	adminController.createQuestion(question1);
    	verify(adminService).createQuestion(question1);
    }

}
