import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.idnoll.controllers.QuizController;
import com.idnoll.models.QuestionModel;

public class checkForDuplicatesTest {
	
	List<QuestionModel> questions;
	QuizController quizController;
	
	@Before
	public void setup(){
		
		quizController = new QuizController();
		questions = new ArrayList<>();

	}
	
	@Test
	public void testWithOneDuplicate() {	
		
		questions.add(new QuestionModel("Vad är 1+1","2","3","4","Matte",0L));
		questions.get(0).setQuestion_id(0L);
		questions.add(new QuestionModel("Vad är 2+2","4","6","8","Matte",0L));
		questions.get(1).setQuestion_id(1L);
		questions.add(new QuestionModel("Vad är 1+1","2","3","4","Matte",0L));
		questions.get(2).setQuestion_id(0L);
		
		assertEquals(2, quizController.checkListForDuplicates(questions).size());
	}
	
	@Test
	public void testWithOneDuplicateAndOneTriplicate() {	
		questions.add(new QuestionModel());
		questions.get(0).setQuestion_id(0L);
		questions.add(new QuestionModel());
		questions.get(1).setQuestion_id(1L);
		questions.add(new QuestionModel());
		questions.get(2).setQuestion_id(0L);
		questions.add(new QuestionModel());
		questions.get(3).setQuestion_id(1L);
		questions.add(new QuestionModel());
		questions.get(4).setQuestion_id(0L);
		
		assertEquals(2, quizController.checkListForDuplicates(questions).size());
	}
	
	@Test
	public void testWithNoDuplicates() {	
		questions.add(new QuestionModel());
		questions.get(0).setQuestion_id(0L);
		
		assertEquals(1, quizController.checkListForDuplicates(questions).size());
	}

}
