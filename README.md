# README #

#General Information

Name of the project: 	QUIZ  by ID0


# Description:		
Appen är en underhållande hemsida som kan förgylla folks vardag
när de behöver fördriva lite tid. Med olika kategorier vill vi skapa en underhållningstjänst för alla olika personligheter och åldrar.

# Technologies:		
The project is en webb-baserad frågesportapplikation utvecklad i 
Java med hjälp av Maven, Spring och MySQL

#Getting Started
Quiz by ID0 is web-based quiz services that allow you to easily create & play online quizes i different categories. Underneath the requirements categories and subcategories you are presented with features such as time limits, public & private access, randomize Questions, instant feedback, multiple choice, short answer & more Quiz types.

Instructions on installing any software the application is dependent on: e.g.: PostgreSQL, Heroku.

Instructions on running the app: For rails apps you’ll want to include the rake db:create db:migrate db:seed process

Instructions on starting a server (e.g. are we using pow, or just the default `rails s`)

# List of credentials that can be used to log in:
admin@quiz.se	admin
user@quiz.se	user

* For a full description of the module, visit the project page:
   https://bitbucket.org/idnollnoll/quizrepo

 * To submit bug reports and feature suggestions, or to track changes:
  https://bitbucket.org/idnollnoll/quizrepo/issues?status=new&status=open

# Testing
Commands to run the test suites (JUnit and Mokito): 
mvn clean test -pl :my-module -Dtest=CustomTest

# Link to change log
https://bitbucket.org/idnollnoll/quizrepo/raw/52662b755a6221b6783efe516f4235f249bf276e/ChangeLog.pdf

# Contact details:		
ID0 Developers:		

Josef Dillner <josefdillner@gmail.com>,

Lennart Ekberg <lennart.ekberg71@bredband.net>, 

Max Geissler <max.j.geissler@gmail.com>,

Luciana Haugen <lucianahaugen@gmail.com>.# My project's README